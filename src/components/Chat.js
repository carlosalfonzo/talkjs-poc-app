/* eslint-disable no-debugger */
/* eslint-disable no-unused-vars */
import { useEffect, useRef, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Talk from "talkjs";
import "./styles.css";

const users = {
  carlos: {
    id: "test-4",
    name: "Carlos",
    email: null,
  },
  ariana: {
    id: "test-5",
    name: "Ariana",
    email: null,
  },
  hanley: {
    id: "test-6",
    name: "Hanley",
    email: null,
  },
};

function ChatsPOC() {
  const chatContainer = useRef();
  const { user, conversationId } = useParams();
  const [LocalConversatId, setConversation] = useState(conversationId);

  async function createUsers() {
    try {
      await Talk.ready;
      const user1 = new Talk.User(users[user]);
      const user2 = new Talk.User(
        user === "carlos" ? users["ariana"] : users["hanley"]
      );
      window.talkSession = new Talk.Session({
        appId: "tCfIpJh9",
        me: user1,
      });
      const converId = !LocalConversatId
        ? Talk.oneOnOneId(user1, user2)
        : LocalConversatId;
      if (!LocalConversatId) {
        setConversation(converId);
      }
      const conversation = window.talkSession.getOrCreateConversation(converId);
      conversation.setParticipant(user1);
      conversation.setParticipant(user2);
      const inbox = window.talkSession.createChatbox(conversation);
      inbox.mount(chatContainer.current);
      debugger;
    } catch (error) {
      debugger;
    }
  }

  useEffect(() => {
    createUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user, conversationId]);

  return (
    <div className="chatbox-container">
      <div
        className="chatbox"
        id="talkjs-container"
        style={{ height: "500px" }}
        ref={chatContainer}
      >
        <i>Loading chat...</i>
      </div>
      <div className="chatbox-header">
        {LocalConversatId && user !== "hanley" && (
          <Link to={`/chat/hanley/${LocalConversatId}`}>
            Get Hanley Into this Chat!
          </Link>
        )}
      </div>
    </div>
  );
}

export default ChatsPOC;
