import { Link } from "react-router-dom";

export default function Home() {
  return (
    <nav>
      <ul>
        <li>
          <Link to="/chat/carlos">Chat as Carlos</Link>
        </li>
        <li>
          <Link to="/chat/ariana">Chat as Ariana</Link>
        </li>
      </ul>
    </nav>
  );
}
